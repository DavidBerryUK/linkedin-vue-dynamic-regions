export default class ProfileModel {
  forename: string;
  surname: string;
  mobileNumber: string;
  landlineNumber: string;
  emailAddress: string;
  street: string;
  town: string;
  city: string;
  postcode: string;

  constructor() {
    this.forename = "Fred";
    this.surname = "Blogs";
    this.mobileNumber = "07710 49492828";
    this.landlineNumber = "01924 10203040";
    this.emailAddress = "fred.blogs@google.com"
    this.street = "The Coding Blogs";
    this.town = "4 Terrace Avenue";
    this.city = "Wakefield";
    this.postcode = "WF1 1FW";
  }
}
