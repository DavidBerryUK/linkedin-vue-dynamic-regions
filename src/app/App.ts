import Component from "vue-class-component";
import IndexedRegionsContainerComponent from "@/components/indexedRegions/IndexedRegionsContainerComponent.vue";
import ModelViewerProfileComponent from "@/components/modelViewerProfile/ModelViewerProfileComponent.vue";
import ProfileAddressComponent from "@/components/profileAddress/ProfileAddressComponent.vue";
import ProfileEmailComponent from "@/components/profileEmail/ProfileEmailComponent.vue";
import ProfileModel from "@/models/ProfileModel";
import ProfileNameComponent from "@/components/profileName/ProfileNameComponent.vue";
import ProfilePhoneComponent from "@/components/profilePhone/ProfilePhoneComponent.vue";
import RegionComponent from "@/components/indexedRegions/RegionComponent.vue";
import TabBarComponent from "@/components/tabBar/TabBarComponent.vue";
import TabItemModel from "@/components/tabBar/models/TabItemModel";
import Vue from "vue";

@Component([
  IndexedRegionsContainerComponent,
  ProfileAddressComponent,
  ProfileEmailComponent,
  ProfileNameComponent,
  ProfilePhoneComponent,
  RegionComponent,
  TabBarComponent,
  ModelViewerProfileComponent
])
export default class AppComponent extends Vue {
  categories: Array<TabItemModel> = new Array<TabItemModel>();
  selectedIndex = 0;
  profile = new ProfileModel();

  mounted() {
    this.createTabItems();
  }

  private createTabItems() {
    this.categories = [
      new TabItemModel("n", "Name"),
      new TabItemModel("p", "Phone"),
      new TabItemModel("a", "address"),
      new TabItemModel("e", "email")
    ];
  }

  onTabIndexChanged(index: number, tab: TabItemModel) {
    this.selectedIndex = index;
  }
}

Vue.component("app", AppComponent);
