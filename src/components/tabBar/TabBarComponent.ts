import { Prop, Watch } from "vue-property-decorator";
import Component from "vue-class-component";
import TabItemModel from "./models/TabItemModel";
import Vue from "vue";

@Component
export default class TabBarComponent extends Vue {
  private actualSelectedIndex = 0;
  private actualTabItems: Array<TabItemModel> = new Array<TabItemModel>();

  @Prop()
  items!: Array<TabItemModel>;

  @Prop()
  selectedIndex!: number;

  @Watch("items")
  private onItemsUpdated() {
    this.onPropertyUpdatedItems();
  }

  @Watch("selectedIndex")
  private onSelectedIndexUpdated() {
    this.onPropertyUpdatedSelectedIndex();
  }

  /**********************************
   * Component Lifecycle
   *********************************/
  mounted() {
    this.onPropertyUpdatedSelectedIndex();
    this.onPropertyUpdatedItems();
  }

  /**********************************
   * Component Event Handlers
   *********************************/
  private onTabClick(index: number, tab: TabItemModel) {
    this.actualSelectedIndex = index;
    this.$emit("onIndexChanged", this.actualSelectedIndex, tab);
  }

  /**********************************
   * Component Render Helpers
   *********************************/
  private createTabStyles(index: number): string {
    if (index === this.actualSelectedIndex) {
      return "tab selected";
    }
    return "tab";
  }

  /**********************************
   * Property Update Event Handlers
   *********************************/
  private onPropertyUpdatedSelectedIndex() {
    if (this.selectedIndex) {
      this.actualSelectedIndex = this.selectedIndex;
    }
  }
  private onPropertyUpdatedItems() {
    if (this.items) {
      this.actualTabItems = this.items;
    }
  }
}

Vue.component("db-tab-bar", TabBarComponent);
