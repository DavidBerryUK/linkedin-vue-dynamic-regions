import { Prop } from "vue-property-decorator";
import Component from "vue-class-component";
import ProfileModel from "@/models/ProfileModel";
import Vue from "vue";

@Component
export default class ProfilePhoneComponent extends Vue {
  @Prop()
  value!: ProfileModel;
}

Vue.component("db-profile-phone", ProfilePhoneComponent);
