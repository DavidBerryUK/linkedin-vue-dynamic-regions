import { Prop } from "vue-property-decorator";
import Component from "vue-class-component";
import ProfileModel from "@/models/ProfileModel";
import Vue from "vue";

@Component
export default class ProfileAddressComponent extends Vue {
  @Prop()
  value!: ProfileModel;
}

Vue.component("db-profile-address", ProfileAddressComponent);
