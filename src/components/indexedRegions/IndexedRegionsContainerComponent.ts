import { Prop, Watch } from "vue-property-decorator";
import Component from "vue-class-component";
import RegionComponent from "./RegionComponent";
import Vue from "vue";

@Component
export default class IndexedRegionsContainerComponent extends Vue {
  @Prop()
  regionVisible!: number;

  @Watch("regionVisible")
  onRegionVisibleUpdated() {
    this.onPropertyUpdatedRegionVisible();
  }

  mounted() {
    this.onPropertyUpdatedRegionVisible();
  }

  private onPropertyUpdatedRegionVisible() {
    this.showVisibleRegion();
  }

  private showVisibleRegion() {
    this.$children.forEach((child, index) => {
      if (child.$options.name === "RegionComponent") {
        const region = child as RegionComponent;
        if (index === this.regionVisible) {
          region.show();
        } else {
          region.hide();
        }
      }
    });
  }
}

Vue.component("db-indexed-regions-container", IndexedRegionsContainerComponent);
