import Component from "vue-class-component";
import Vue from "vue";

@Component
export default class RegionComponent extends Vue {
  private isVisible = false;

  show() {
    this.isVisible = true;
  }

  hide() {
    this.isVisible = false;
  }
}

Vue.component("db-region", RegionComponent);
